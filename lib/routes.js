"use strict";

// this is the controller function used as the handler for our Home page
const Home = require("./controllers/home");

// this contains all the controller functions for the note CRUD operations
const Note = require("./controllers/note");

const Path = require('path');

module.exports = [
  // this defines the home route for get all
  {
    method: "GET",
    path: "/",
    handler: Home,
    config: {
      description: "Gets all the notes available"
    }
  },
  // this is the add route
  // note that parameters can be named within curly braces, e.g. {slug}
  {
    method: "POST",
    path: "/note",
    handler: Note.create,
    config: {
      description: "adds new note"
    }
  },
  // show a note
  {
    method: "GET",
    path: "/note/{slug}",
    handler: Note.read,
    config: {
      description: "gets contents of the note"
    }
  },
  // update a note
  {
    method: "PUT",
    path: "/note/{slug}",
    handler: Note.update,
    config: {
      description: "updates the selected note"
    }
  },
  // delete a note
  {
    method: "GET",
    path: "/note/{slug}/delete",
    handler: Note.delete,
    config: {
      description: "deletes the selected note"
    }
  },
  {
    // static files
    // more info: https://hapijs.com/tutorials/serving-files?lang=en_US#directory-handler
    method: 'GET',
    path: '/{param*}',
    // instead of a function, we give it an object with the directory that we want to make public
    handler: {
      directory: {
        path: Path.join(__dirname, '../static/public')
      }
    },
    config: {
      description: 'Provides static resources'
    }
  }
];
