/**
 * This is the controller for the home page
 * 
 * it will list all of the notes in descending order of date (most recent first)
 */

"use strict";

const Models = require("../models/");

module.exports = (request, reply) => {
  Models.Note
    // this is a method of sequelize that allows us to get all of the Note objects
    // then we order by date descending
    // all options can be found here: http://docs.sequelizejs.com/en/latest/api/model/#findalloptions-promisearrayinstance
    .findAll({
      order: [["date", "DESC"]]
    })
    .then(result => {
      // currently we are just replying with JSON
      // we can either leave it like that and have the client transform that into HTML,
      // or reply with an HTML block here
      // reply({
      //   data: {
      //     notes: result
      //   },
      //   page: "Home - Notes Board",
      //   description: "Welcome to my notes board"
      // });

      // the view method is available thanks to the vision plugin
      // it is now able to find views that we've added to the lib/views directory
      reply.view("home", {
        data: {
          notes: result
        },
        page: "Home - Notes Board",
        descroption: "Welcome to my notes board"
      });
    });
};
