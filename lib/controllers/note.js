/**
 * this contains all the CRUD operations for an individual note
 */

"use strict";

const Models = require("../models/");
const Slugify = require("slug");
const Path = require("path");
const Pug = require("pug");

module.exports = {
  // Here we are going to include our functions that will handle each request in the routes.js file.
  create: (request, reply) => {
    Models.Note
      .create({
        // this is the raw data being stored in the note's 'date' column
        date: new Date(),
        title: request.payload.noteTitle,
        slug: Slugify(request.payload.noteTitle, { lower: true }),
        description: request.payload.noteDescription,
        content: request.payload.noteContent
      })
      .then(result => {
        // we can generate a view later - for now, just return JSON result
        // reply(result);

        // we're going to pre-render the html on the server and send that to the client
        // we could also include pug and render the json on the client
        const newNote = Pug.renderFile(
          Path.join(__dirname, "../views/components/note.pug"),
          {
            note: result
          }
        );

        reply(newNote);
      });
  },
  read: (request, reply) => {
    Models.Note
      .findOne({
        where: {
          slug: request.params.slug
        }
      })
      .then(result => {
        reply.view("note", {
          note: result,
          page: `${result.title} - Notes Board`,
          description: result.description
        });
      });
  },
  /*
    To update a note, we use the update method on our model. It takes two objects, the new values that we are 
    going to replace and the options containing a where filter with the note slug, which is the note that we are 
    going to update
    */
  update: (request, reply) => {
    const values = {
      title: request.payload.noteTitle,
      description: request.payload.noteDescription,
      content: request.payload.noteContent
    };

    const findOptions = {
      where: {
        slug: request.params.slug
      }
    };

    const updateOptions = Object.assign({}, findOptions, {
      returning: true,
      fields: [
        'title', 'description', 'content'
      ]
    });

    /*
        Since the update method doesn't return the new data, we have to fetch the updated data after we're done
        and return it.
        */
    Models.Note.update(values, updateOptions).then(() => {
      Models.Note.findOne(findOptions).then(result => {
        const updatedNote = Pug.renderFile(
          Path.join(__dirname, "../views/components/note.pug"),
          {
            note: result
          }
        );

        reply(updatedNote);
      });
    });
  },
  delete: (request, reply) => {
    Models.Note
      .destroy({
        where: {
          slug: request.params.slug
        }
      })
      // redirect to homepage after it is deleted since we can't see it anymore
      // also, consider making this a softdelete?
      // or, adding an 'archive' function that softdeletes?
      .then(() => {
        reply.redirect("/");
      });
  }
};
