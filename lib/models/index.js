/**
 * This file basically gets all the other files in this directory and 
 * imports them into sequelize.
 * 
 * So, while this file itself doesn't exactly do much, it essentially enables us 
 * to put other routes in place and import them all seamlessly
 */

"use strict";

const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const Settings = require("../../settings");

// database settings for current environment
const dbSettings = Settings[Settings.env].db;

const sequelize = new Sequelize(
  dbSettings.database,
  dbSettings.user,
  dbSettings.password,
  dbSettings
);
const db = {};

// read all the files in this dir and import them as models
fs
  .readdirSync(__dirname)
  .filter(file => file.indexOf(".") !== 0 && file !== "index.js")
  .forEach(file => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

// add our new database instance to the db object
db.sequelize = sequelize;
// I think this is just a convenience so we don't have to worry about requiring it in every file?
db.Sequelize = Sequelize;

module.exports = db;
