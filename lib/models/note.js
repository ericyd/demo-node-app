/**
 * this contains the model for the notes
 * It includes methods for all CRUD operations
 */

"use strict";

const Moment = require("moment");

// I think the arguments to this model are determined by the Sequelize library
// the DataTypes object will contain all types available to our database
module.exports = (sequelize, DataTypes) => {
  // this defines the structure of our data
  // each key corresponds to a column in the database table
  // for the date, the `get` function determines how the data will be returned, but it should
  // be noted that the raw data is still a JS Date() object, no the formatted string
  const Note = sequelize.define("Note", {
    date: {
      type: DataTypes.DATE,
      get: function() {
        return Moment(this.getDataValue("date")).format("MMMM Do, YYYY");
      }
    },
    title: DataTypes.STRING,
    slug: DataTypes.STRING,
    description: DataTypes.STRING,
    content: DataTypes.STRING
  });

  // this returns the Note model
  return Note;
};
