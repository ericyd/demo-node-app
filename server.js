"use strict";

const Hapi = require("hapi");
const Hoek = require("hoek");
const Settings = require("./settings");
// this imports the 'index.js'
const Models = require("./lib/models/");
// get the routes for the server
const Routes = require("./lib/routes");
const path = require("path");

const server = new Hapi.Server();
server.connection({ port: Settings.port });

server.register(
  [
    // registering vision procudes the view functionality
    require("vision"),
    // registering inert allows static files to be served
    require("inert")
  ],
  err => {
    Hoek.assert(!err, err);

    // this controls the view settings, but the views are declared in the corresponding controllers
    server.views({
      engines: { pug: require("pug") },
      path: path.join(__dirname, "lib/views"),
      compileOptions: {
        pretty: false
      },
      isCached: Settings.env === "production"
    });

    server.route(Routes);
  }
);

// this will first sync the database, then start the server
// remember, models/index.js exports sequelize, which is an instance of Sequlize that
// represents our database
Models.sequelize.sync().then(() => {
  server.start(err => {
    Hoek.assert(!err, err);

    console.log(`Server running at: ${server.info.uri}`);
  });
});
