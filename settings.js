require("dotenv").config({ silent: true });

module.exports = {
  port: process.env.PORT,
  env: process.env.ENV,

  // environment-specific settings
  development: {
    db: {
      dialect: "sqlite",
      storage: ":memory:"
    }
  },
  production: {
    db: {
      dialect: "sqlite",
      storage: "db/database.sqlite"
    }
  }
};
