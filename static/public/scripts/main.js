document.getElementById('note-form').addEventListener('submit', noteFormSubmit, false);

function noteFormSubmit (e) {
    e.preventDefault();
    var self = e.target;

    var form = {
        url: self.getAttribute('action'),
        type: self.getAttribute('method')
    };

    var formData = {
        noteContent: self.noteContent.value,
        noteTitle: self.noteTitle.value,
        noteDescription: self.noteDescription.value
    };

    axios({
        method: form.type,
        url: form.url,
        data: formData
    })
    .then(function(response) {
        if (form.type === 'POST') {
            var noteList = document.querySelector('.notes-list');
            noteList.innerHTML = response.data + noteList.innerHTML;
        } else if (form.type === 'PUT') {
            document.querySelector('.note').innerHTML = response.data;
        }
    })
    .catch(function(err) {
        console.log(err);
    });
}
